import '../client/css/custom.scss';
import '../client/css/header.scss';
import '@fortawesome/fontawesome-free/css/all.min.css'; 
import 'bootstrap-css-only/css/bootstrap.min.css';

function Header() {
  return (
    <header className="header">
      <a href="/" className="brand">URL Shortner</a>
      <ul>
        <li><a href="https://bitbucket.org/shubham1806/url-shortner/src/master/" rel="noreferrer" target="_blank">Go to repo</a></li>
      </ul>
    </header>
  )
}

export default Header;
