import '../client/css/custom.scss';
import '../client/css/footer.scss';
import '@fortawesome/fontawesome-free/css/all.min.css'; 
import 'bootstrap-css-only/css/bootstrap.min.css';

function Footer() {
  return (
    <footer>
        <p>Created by <a href="https://www.linkedin.com/in/shubham-sharma-2491b811a/" rel="noreferrer" target="_blank">Shubham Sharma</a></p>
    </footer>
  )
}

export default Footer;
