import '../client/css/custom.scss';
import React from 'react';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';

var date = new Date();
var defaultExpiryString = new Date(date.getTime() + (7 * 24 * 60 * 60 * 1000)).toISOString().split("T")[0];
var today = date.toISOString().split("T")[0];
console.log(defaultExpiryString);

// Dynamic API End Point
var API_END_POINT = (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') ? "http://localhost:5000" : "https://shortmyurl-backend-72e27ed52364.herokuapp.com"
class Body extends React.Component {
  constructor() {
    super();
    this.state = {
      url: "",
      expiresOn: defaultExpiryString,
      loading: false,
      loaded: false,
      shortURL: "",
      isCopied: false,
      copyBtnHTML: `Copy <i class="fa fa-copy"></i>`,
      isValidSubmit: true
    }
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleSubmitBtnHTML = this.handleSubmitBtnHTML.bind(this);
    this.handleResultPanel = this.handleResultPanel.bind(this);
    this.handleCopyBtn = this.handleCopyBtn.bind(this);
    this.copyToClipboard = this.copyToClipboard.bind(this);
    this.handleError = this.handleError.bind(this);
  }


  handleInputChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }
  handleSubmit(event) {
    if(this.state.url === "") {
      this.setState({
        isValidSubmit: false
      })
    } else {
      var preparedQuery = {
        "url": this.state.url,
        "expiresOn": this.state.expiresOn
      }
      if(this.state.url.indexOf("http://") === -1 && this.state.url.indexOf("https://") === -1) {
        preparedQuery.url = "http://"+preparedQuery.url;
      }
      this.setState({
        loading: true,
        isValidSubmit: true
      });
      fetch(API_END_POINT+"/shortLink?url=" + preparedQuery.url + "&expiresOn=" + preparedQuery.expiresOn)
        .then(res => res.json())
        .then((result) => {
          console.log(result);
          this.setState({
            loading: false,
            loaded: true,
            shortURL: window.location.protocol+"//"+window.location.host + result.shortLink,
            isCopied: false,
            copyBtnHTML: `Copy <i class="fa fa-copy"></i>`,
          })
          console.log(this);
        }, (error) => console.log(error));
    }
    
    event.preventDefault();
  }

  handleSubmitBtnHTML() {
    console.log(this.state.loading)
    if (this.state.loading) {
      return (
        <span>Loading <i className="fas fa-cog fa-spin"></i></span>
      )
    } else {
      return (
        <span>Short it</span>
      )
    }
  }
  handleResultPanel() {
    console.log(this.state);
    if (this.state.loaded) {
      return (
        <div className="resultPanel">
          <p id="shortLinkContainer">{this.state.shortURL}</p>
          <textarea id="shortLinkTextArea" defaultValue={this.state.shortURL}></textarea>
          <button type="button" id="cpyBtn" className="cpyBtn btn btn-default" onClick={this.handleCopyBtn} dangerouslySetInnerHTML={{ __html: this.state.copyBtnHTML }}></button>
        </div>
      )
    }
  }

  handleCopyBtn(e) {
    console.log(this.state, typeof e);
    if (!this.state.isCopied) {
      this.copyToClipboard()
      this.setState({
        copyBtnHTML: "Copied !"
      })
    } else {
      this.setState({
        copyBtnHTML: `Copy <i class="fa fa-copy"></i>`
      });
    }

  }

  copyToClipboard(e) {
    document.getElementById("shortLinkTextArea").select();
    var copy = document.execCommand('copy');
    this.setState({
      isCopied: copy
    })
  }

  handleError() {
    if(!this.state.isValidSubmit) {
      return ("error")
    } else {
      return ("");
    }
  }

  render() {
    return (
      <div className="content_wrapper">
        <div className="headline"><h1>Want your life easy ?</h1></div>
        <form className="linkShorteningForm" action="#" onSubmit={this.handleSubmit}>
          <input type="text" name="url" value={this.state.url}
            onChange={this.handleInputChange}
            className={`form-control linkInput ${this.handleError()}`}
            placeholder="Enter link to short" />
          <label className="expireLabel">Expires on</label>
          <input type="date" name="expiresOn" min={today} max="2028-12-31"
            value={this.state.expiresOn}
            onChange={this.handleInputChange} className="form-control dateInput" />
          <button type="submit" className="submitBtn btn btn-primary">{this.handleSubmitBtnHTML()}</button>
        </form>
        <div>{this.handleResultPanel()}</div>
      </div>
    )
  }
}

export default Body;
