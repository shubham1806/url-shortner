import React from 'react';
import './client/css/custom.scss';

// Dynamic API End Point
var API_END_POINT = (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') ? "http://localhost:5000" : "https://url-short-link-backend.herokuapp.com"
class Redirect extends React.Component {
    constructor() {
        super();
        this.state = {
            shortLinkPath: window.location.pathname,
            urlToRoute: ""
        }
    }

    componentDidMount() {
       
        this.setState({
            shortLinkPath: window.location.pathname
        });
        console.log(this.state);
        fetch(API_END_POINT+"/getLink?shortLink=" + this.state.shortLinkPath)
        .then((res) => {console.log(res,res.status); return res.json()})
        .then((result) => {
            console.log(result);
            this.setState({urlToRoute: result.url});
        });
        // setTimeout(()=> {
        //     window.location.href = this.state.urlToRoute;
        // },1000)
        window.location.href = this.state.urlToRoute;
    }
    render() {
        return (
            <h2 className="redirectLanding">You are being redirected to <a href={this.state.urlToRoute}>{this.state.urlToRoute}</a></h2>
        )
    }
}

export default Redirect;