import './client/css/custom.scss';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';

// Importing Components
import Header from "./components/Header";
import Body from "./components/Body";
import Footer from "./components/Footer";

function App() {
  return (
    <div>
      <Header />
      <Body />
      <Footer />
    </div>
  )
}

export default App;
